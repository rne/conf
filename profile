export PATH=/usr/local/bin:$PATH
PS1="\w >>"
export LANG="da_DK.UTF-8"
export LC_COLLATE="da_DK.UTF-8"
export LC_CTYPE="da_DK.UTF-8"
export LC_MESSAGES="da_DK.UTF-8"
export LC_MONETARY="da_DK.UTF-8"
export LC_NUMERIC="da_DK.UTF-8"
export LC_TIME="da_DK.UTF-8"
export LC_ALL=

won () {
  if [ "$1" = "" ]; then
    deactivate
    unset PRODIR
    echo "]0;"
    cd
  elif [ -d /projects/$1 ]; then
    PRODIR=/projects/$1
    [ -d $PRODIR/bin ] && {
      source $PRODIR/bin/activate
    }
    echo "]0;$1"
    PS1="[$1] $PS1"
    cd $PRODIR
  fi
}


